//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 Sandia Corporation.
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_worklet_ExternalFacesHashFightMorton_h
#define vtk_m_worklet_ExternalFacesHashFightMorton_h

#include <vtkm/CellShape.h>
#include <vtkm/Math.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/cont/ArrayHandleImplicit.h>
#include <vtkm/cont/ArrayHandleIndex.h>
#include <vtkm/cont/ArrayHandlePermutation.h>
#include <vtkm/cont/CellSetExplicit.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/Field.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/exec/CellFace.h>

#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/ScatterCounting.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/WorkletMapTopology.h>

#include "MortonCodes.h"
#include "YamlWriter.h"

namespace vtkm
{
namespace worklet
{

struct ExternalFacesHashFightMorton
{
  //Unary predicate operator
  //Returns True if the argument is equal to the constructor
  //integer argument; False otherwise.
  struct IsIntValue
  {
  private:
    int Value;

  public:
    VTKM_EXEC_CONT
    IsIntValue(const int &v) : Value(v) { }

    template<typename T>
    VTKM_EXEC_CONT
    bool operator()(const T &x) const
    {
      return x == T(Value);
    }
  };

  // Given a cell shape, face index, and vec of cell connection ids,
  // returns an Id3 that is guaranteed to be unique and matching for
  // each face.
  template<typename CellShapeTag,
           typename CellNodeVecType>
  VTKM_EXEC
  static
  vtkm::Id3 CanonicalFaceId(CellShapeTag shape,
                            vtkm::IdComponent faceIndex,
                            const CellNodeVecType &cellNodeIds,
                            const vtkm::worklet::internal::WorkletBase *self)
  {
    vtkm::VecCConst<vtkm::IdComponent> localFaceIndices =
        vtkm::exec::CellFaceLocalIndices(faceIndex, shape, *self);

    VTKM_ASSERT(localFaceIndices.GetNumberOfComponents() >= 3);

    //Assign cell points/nodes to this face
    vtkm::Id faceP1 = cellNodeIds[localFaceIndices[0]];
    vtkm::Id faceP2 = cellNodeIds[localFaceIndices[1]];
    vtkm::Id faceP3 = cellNodeIds[localFaceIndices[2]];

    //Sort the first 3 face points/nodes in ascending order
    vtkm::Id sorted[3] = {faceP1, faceP2, faceP3};
    vtkm::Id temp;
    if (sorted[0] > sorted[2])
    {
      temp = sorted[0];
      sorted[0] = sorted[2];
      sorted[2] = temp;
    }
    if (sorted[0] > sorted[1])
    {
      temp = sorted[0];
      sorted[0] = sorted[1];
      sorted[1] = temp;
    }
    if (sorted[1] > sorted[2])
    {
      temp = sorted[1];
      sorted[1] = sorted[2];
      sorted[2] = temp;
    }

    // Check the rest of the points to see if they are in the lowest 3
    vtkm::IdComponent numPointsInFace =
        localFaceIndices.GetNumberOfComponents();
    for (vtkm::IdComponent pointIndex = 3;
         pointIndex < numPointsInFace;
         pointIndex++)
    {
      vtkm::Id nextPoint = cellNodeIds[localFaceIndices[pointIndex]];
      if (nextPoint < sorted[2])
      {
        if (nextPoint < sorted[1])
        {
          sorted[2] = sorted[1];
          if (nextPoint < sorted[0])
          {
            sorted[1] = sorted[0];
            sorted[0] = nextPoint;
          }
          else // nextPoint > P0, nextPoint < P1
          {
            sorted[1] = nextPoint;
          }
        }
        else // nextPoint > P1, nextPoint < P2
        {
          sorted[2] = nextPoint;
        }
      }
      else // nextPoint > P2
      {
        // Do nothing. nextPoint not in top 3.
      }
    }

    return vtkm::Id3(sorted[0], sorted[1], sorted[2]);
  }

  //Worklet that returns the number of faces for each cell/shape
  class NumFacesPerCell : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn inCellSet,
                                  FieldOut<> numFacesInCell);
    typedef _2 ExecutionSignature(CellShape);
    typedef _1 InputDomain;

    template<typename CellShapeTag>
    VTKM_EXEC
    vtkm::IdComponent operator()(CellShapeTag shape) const
    {
      return vtkm::exec::CellFaceNumberOfFaces(shape, *this);
    }
  };

  //Worklet that computes the Morton code for every point
  class PointMorton : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> pointCoordinates,
                                  FieldOut<> mortonCode);
    typedef _2 ExecutionSignature(_1);
    using InputDomain = _1;

    VTKM_CONT
    PointMorton(const vtkm::Bounds &bounds)
      : Bounds(bounds)
    {  }

    template<typename CoordinateValueType>
    VTKM_EXEC
    vtkm::UInt32 operator()(const vtkm::Vec<CoordinateValueType,3> &coords) const
    {
      return vtkm::Morton32(coords, this->Bounds);
    }

  private:
    vtkm::Bounds Bounds;
  };

  //Worklet that identifies a cell face by an XOR of all point ids. Not
  //necessarily completely unique.
  class FaceHash : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn cellset,
                                  FieldInPoint<> pointMorton,
                                  FieldOut<> faceHashes,
                                  FieldOut<> originCells,
                                  FieldOut<> originFaces);
    typedef void ExecutionSignature(_2,
                                    _3,
                                    _4,
                                    _5,
                                    CellShape,
                                    InputIndex,
                                    VisitIndex);
    typedef _1 InputDomain;

    using ScatterType = vtkm::worklet::ScatterCounting;

    VTKM_CONT
    ScatterType GetScatter() const { return this->Scatter; }

    template<typename CountArrayType, typename Device>
    VTKM_CONT
    FaceHash(const CountArrayType &countArray,
             Device)
      : Scatter(countArray, Device())
    {
      VTKM_IS_ARRAY_HANDLE(CountArrayType);
    }

    VTKM_CONT
    FaceHash(const ScatterType &scatter)
      : Scatter(scatter)
    {  }

    template<typename PointMortonVecType,
             typename CellShapeTag>
    VTKM_EXEC
    void operator()(const PointMortonVecType &pointMorton,
                    vtkm::UInt32 &faceHash,
                    vtkm::Id &cellIndex,
                    vtkm::IdComponent &faceIndex,
                    CellShapeTag shape,
                    vtkm::Id inputIndex,
                    vtkm::IdComponent visitIndex) const
    {
      vtkm::VecCConst<vtkm::IdComponent> localFaceIndices =
          vtkm::exec::CellFaceLocalIndices(visitIndex, shape, *this);

      // The basic idea of the hash is to have the morton code for the point
      // coordinates at the center of the face. What we compute is slightly
      // different though. We are computing the Morton code for each point and
      // then adding them together. This is sort of a sum rather than an
      // average of point coordinates (with some other minor differences), but
      // the real point is that the operation will be the same for all sets of
      // points.
      faceHash = 0;
      vtkm::IdComponent numPointsInFace =
          localFaceIndices.GetNumberOfComponents();
      for (vtkm::IdComponent pointIndex = 0;
           pointIndex < numPointsInFace;
           pointIndex++)
      {
        vtkm::IdComponent localPointId = localFaceIndices[pointIndex];
        vtkm::UInt32 pointHash = pointMorton[localPointId];
        faceHash += pointHash;
      }

      cellIndex = inputIndex;
      faceIndex = visitIndex;
    }

  private:
    ScatterType Scatter;
  };

  //Worklet that writes the face index at the location of the hash table.
  //Multiple entries are likely to write to the hash table, so they fight
  //and (hopefully) one wins.
  class HashFight : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> Hashes,
                                  FieldIn<> FaceIds,
                                  WholeArrayOut<> HashTable);
    typedef void ExecutionSignature(_1, _2, _3);

    VTKM_CONT
    HashFight(vtkm::Id hashTableSize)
      : HashTableSize(hashTableSize)
    {  }

    template<typename HashTablePortalType>
    VTKM_EXEC
    void operator()(vtkm::Id hash,
                    vtkm::Id faceId,
                    HashTablePortalType &hashTablePortal) const
    {
      hashTablePortal.Set(hash%this->HashTableSize, faceId);
    }

  private:
    vtkm::Id HashTableSize;
  };

  //Worklet that detects whether a face is internal.  If the
  //face is internal, then a value should not be assigned to the
  //face in the output array handle of face vertices; only external
  //faces should have a vector not equal to <-1,-1,-1>
  class CheckForMatches : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> activeHashes,
                                  FieldIn<> activeFaceIndices,
                                  WholeCellSetIn<> cellSet,
                                  WholeArrayIn<> originCells,
                                  WholeArrayIn<> originFaces,
                                  WholeArrayIn<> hashTable,
                                  FieldInOut<> isInactive,
                                  WholeArrayInOut<> isExternalFace);
    typedef void ExecutionSignature(_1, _2, _3, _4, _5, _6, _7, _8);

    VTKM_CONT
    CheckForMatches(vtkm::Id hashTableSize)
      : HashTableSize(hashTableSize)
    {  }

    template<typename CellSetType,
             typename OriginCellsPortal,
             typename OriginFacesPortal,
             typename HashTablePortal,
             typename IsExternalFacePortal>
    VTKM_EXEC
    void operator()(vtkm::UInt32 hash,
                    vtkm::Id faceIndex,
                    const CellSetType &cellSet,
                    const OriginCellsPortal &originCellsPortal,
                    const OriginFacesPortal &originFacesPortal,
                    const HashTablePortal &hashTablePortal,
                    vtkm::UInt8 &isInactive,
                    IsExternalFacePortal &isExternalFacePortal) const
    {
      vtkm::Id hashWinnerFace = hashTablePortal.Get(hash%this->HashTableSize);

      if (hashWinnerFace == faceIndex)
      {
        // Case 1: I won the hash fight by writing my index. I'm done so mark
        // myself as inactive.
        isInactive = vtkm::UInt8(1);
      }
      else
      {
        // Get a cononical representation of my face.
        vtkm::Id myOriginCell = originCellsPortal.Get(faceIndex);
        vtkm::Id myOriginFace = originFacesPortal.Get(faceIndex);
        vtkm::Id3 myFace = CanonicalFaceId(cellSet.GetCellShape(myOriginCell),
                                           myOriginFace,
                                           cellSet.GetIndices(myOriginCell),
                                           this);

        // Get a cononical representation of the face in the hash table.
        vtkm::Id otherOriginCell = originCellsPortal.Get(hashWinnerFace);
        vtkm::Id otherOriginFace = originFacesPortal.Get(hashWinnerFace);
        vtkm::Id3 otherFace =
            CanonicalFaceId(cellSet.GetCellShape(otherOriginCell),
                            otherOriginFace,
                            cellSet.GetIndices(otherOriginCell),
                            this);

        // See if these are the same face
        if (myFace == otherFace)
        {
          // Case 2: The faces are the same. This must be an internal face.
          // Mark both myself and the other face as internal.
          isInactive = vtkm::UInt8(1);
          isExternalFacePortal.Set(faceIndex, vtkm::UInt8(0));
          isExternalFacePortal.Set(hashWinnerFace, vtkm::UInt8(0));
        }
        else
        {
          // Case 3: I didn't win and my face didn't match. I didn't learn
          // anything so do nothing.
        }
      }
    }

  private:
    vtkm::Id HashTableSize;
  };

  // Worklet that counts the number of points that are in each (active) face.
  class NumPointsPerFace : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> faceIndices,
                                  WholeCellSetIn<> cellSet,
                                  WholeArrayIn<> originCells,
                                  WholeArrayIn<> originFaces,
                                  FieldOut<> numPointsInFace);
    typedef _5 ExecutionSignature(_1, _2, _3, _4);

    using ScatterType = vtkm::worklet::ScatterCounting;

    VTKM_CONT
    ScatterType GetScatter() const { return this->Scatter; }

    VTKM_CONT
    NumPointsPerFace(const ScatterType &scatter)
      : Scatter(scatter)
    {  }

    template<typename CellSetType,
             typename OriginCellsPortalType,
             typename OriginFacesPortalType>
    VTKM_EXEC
    vtkm::IdComponent operator()(vtkm::Id faceIndex,
                                 const CellSetType &cellSet,
                                 const OriginCellsPortalType &originCellsPortal,
                                 const OriginFacesPortalType &originFacesPortal
                                 ) const
    {
      vtkm::Id originCell = originCellsPortal.Get(faceIndex);
      vtkm::IdComponent originFace = originFacesPortal.Get(faceIndex);
      return vtkm::exec::CellFaceNumberOfPoints(
            originFace, cellSet.GetCellShape(originCell), *this);
    }

  private:
    ScatterType Scatter;
  };

  // Worklet that writes out the shape and indices for each (active) face.
  class BuildConnectivity : public vtkm::worklet::WorkletMapField
  {
  public:
    typedef void ControlSignature(FieldIn<> faceIndices,
                                  WholeCellSetIn<> cellSet,
                                  WholeArrayIn<> originCells,
                                  WholeArrayIn<> originFaces,
                                  FieldOut<> shapesOut,
                                  FieldOut<> connectivityOut);
    typedef void ExecutionSignature(_1, _2, _3, _4, _5, _6);

    using ScatterType = vtkm::worklet::ScatterCounting;

    VTKM_CONT
    ScatterType GetScatter() const { return this->Scatter; }

    VTKM_CONT
    BuildConnectivity(const ScatterType &scatter)
      : Scatter(scatter)
    {  }

    template<typename CellSetType,
             typename OriginCellsPortalType,
             typename OriginFacesPortalType,
             typename ConnectivityType>
    VTKM_EXEC
    void operator()(vtkm::Id faceIndex,
                    const CellSetType &cellSet,
                    const OriginCellsPortalType &originCellsPortal,
                    const OriginFacesPortalType &originFacesPortal,
                    vtkm::UInt8 &shapeOut,
                    ConnectivityType &connectivityOut) const
    {
      vtkm::Id originCell = originCellsPortal.Get(faceIndex);
      vtkm::IdComponent originFace = originFacesPortal.Get(faceIndex);

      shapeOut = vtkm::exec::CellFaceShape(originFace,
                                           cellSet.GetCellShape(originCell),
                                           *this);

      vtkm::VecCConst<vtkm::IdComponent> localFaceIndices =
          vtkm::exec::CellFaceLocalIndices(originFace,
                                           cellSet.GetCellShape(originCell),
                                           *this);
      vtkm::IdComponent numFacePoints =localFaceIndices.GetNumberOfComponents();
      VTKM_ASSERT(numFacePoints == connectivityOut.GetNumberOfComponents());

      typename CellSetType::IndicesType inCellIndices =
          cellSet.GetIndices(originCell);

      for (vtkm::IdComponent facePointIndex = 0;
           facePointIndex < numFacePoints;
           facePointIndex++)
      {
        connectivityOut[facePointIndex] =
            inCellIndices[localFaceIndices[facePointIndex]];
      }
    }

  private:
    ScatterType Scatter;
  };

public:

  ///////////////////////////////////////////////////
  /// \brief ExternalFacesHashFightMorton: Extract Faces on outside of geometry
  template <typename InCellSetType,
            typename PointCoordsType,
            typename ShapeStorage,
            typename NumIndicesStorage,
            typename ConnectivityStorage,
            typename OffsetsStorage,
            typename DeviceAdapter>
  VTKM_CONT
  void Run(const InCellSetType &inCellSet,
           const PointCoordsType &pointCoords,
           vtkm::cont::CellSetExplicit<
             ShapeStorage,
             NumIndicesStorage,
             ConnectivityStorage,
             OffsetsStorage> &outCellSet,
           YamlWriter &log,
           DeviceAdapter)
  {
    using Algorithm = vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;

    // Not the best place to call GetBounds. Probably should eventually get
    // it passed in.
    vtkm::Bounds bounds = pointCoords.GetBounds();

    //Create a worklet to map the number of faces to each cell
    vtkm::cont::ArrayHandle<vtkm::IdComponent> facesPerCell;
    vtkm::worklet::DispatcherMapTopology<NumFacesPerCell,DeviceAdapter>
        numFacesDispatcher;

    vtkm::cont::Timer<DeviceAdapter> timer;
    numFacesDispatcher.Invoke(inCellSet, facesPerCell);
    log.AddDictionaryEntry("seconds-num-faces-per-cell",
                           timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCellToFace(facesPerCell, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-input-count", timer.GetElapsedTime());
    facesPerCell.ReleaseResources();

    if (scatterCellToFace.GetOutputRange(inCellSet.GetNumberOfCells()) == 0)
    {
      // Data has no faces. Output is empty.
      outCellSet.PrepareToAddCells(0, 0);
      outCellSet.CompleteAddingCells(inCellSet.GetNumberOfPoints());
      return;
    }

    vtkm::cont::ArrayHandle<vtkm::UInt32> pointMorton;
    timer.Reset();
    vtkm::worklet::DispatcherMapField<PointMorton,DeviceAdapter>
        pointMortonDispatcher((PointMorton(bounds)));
    pointMortonDispatcher.Invoke(pointCoords, pointMorton);
    log.AddDictionaryEntry("seconds-morton-codes", timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::UInt32> faceHashes;
    vtkm::cont::ArrayHandle<vtkm::Id> originCells;
    vtkm::cont::ArrayHandle<vtkm::IdComponent> originFaces;
    vtkm::worklet::DispatcherMapTopology<FaceHash,DeviceAdapter>
        faceHashDispatcher((FaceHash(scatterCellToFace)));

    timer.Reset();
    faceHashDispatcher.Invoke(
          inCellSet, pointMorton, faceHashes, originCells, originFaces);
    log.AddDictionaryEntry("seconds-face-hash", timer.GetElapsedTime());
    pointMorton.ReleaseResources();

    vtkm::Id totalNumFaces = faceHashes.GetNumberOfValues();

    //----Begin Hashing Phase To Detect External Faces------------------//

    timer.Reset();

    //Set constant factor for hash table size: factor*totalFaces
    const vtkm::Id hashTableFactor = 2;

    vtkm::cont::ArrayHandle<vtkm::UInt8> isExternalFace;
    Algorithm::Copy(
          vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(1, totalNumFaces),
          isExternalFace);

    vtkm::cont::ArrayHandle<vtkm::Id> activeFaceIndices;
    Algorithm::Copy(vtkm::cont::ArrayHandleIndex(totalNumFaces),
                    activeFaceIndices);

    vtkm::Id numActiveFaces = totalNumFaces;

    while (numActiveFaces > 0)
    {
      // Create a packe arrays of active face hashes
      auto activeHashes = vtkm::cont::make_ArrayHandlePermutation(activeFaceIndices, faceHashes);

      // Get ready the isInactive array.
      vtkm::cont::ArrayHandle<vtkm::UInt8> isInactive;
      Algorithm::Copy(
            vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(0, numActiveFaces),
            isInactive);

      vtkm::Id hashTableSize = numActiveFaces * hashTableFactor;

      vtkm::cont::ArrayHandle<vtkm::Id> hashTable;
      hashTable.PrepareForOutput(hashTableSize, DeviceAdapter());

      // Have all active hashes try to write their index to the hash table
      vtkm::worklet::DispatcherMapField<HashFight,DeviceAdapter>
          fightDispatcher((HashFight(hashTableSize)));
      fightDispatcher.Invoke(activeHashes, activeFaceIndices, hashTable);


      // Have all active faces check to see if they matched and update
      // isInactive/isExternalFace.
      vtkm::worklet::DispatcherMapField<CheckForMatches,DeviceAdapter>
          matchDispatcher((CheckForMatches(hashTableSize)));
      matchDispatcher.Invoke(activeHashes,
                             activeFaceIndices,
                             inCellSet,
                             originCells,
                             originFaces,
                             hashTable,
                             isInactive,
                             isExternalFace);

      // Compact the activeFaceIndices by the isInactive flag.
      vtkm::cont::ArrayHandle<vtkm::Id> compactedActiveFaceIndices;
      Algorithm::CopyIf(activeFaceIndices,
                        isInactive,
                        compactedActiveFaceIndices,
                        IsIntValue(0));
      activeFaceIndices = compactedActiveFaceIndices;

      // Update the number of active faces
      numActiveFaces = activeFaceIndices.GetNumberOfValues();
    }

    log.AddDictionaryEntry("seconds-hash-fight-iterations",
                           timer.GetElapsedTime());

    //--------------End Hashing Phase-------------------------//

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCullInternalFaces(isExternalFace, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-output-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::IdComponent,NumIndicesStorage> facePointCount;
    vtkm::worklet::DispatcherMapField<NumPointsPerFace,DeviceAdapter>
        pointsPerFaceDispatcher((NumPointsPerFace(scatterCullInternalFaces)));

    timer.Reset();
    pointsPerFaceDispatcher.Invoke(vtkm::cont::ArrayHandleIndex(totalNumFaces),
                                   inCellSet,
                                   originCells,
                                   originFaces,
                                   facePointCount);
    log.AddDictionaryEntry("seconds-points-per-face",
                           timer.GetElapsedTime());

    vtkm:cont::ArrayHandle<vtkm::UInt8,ShapeStorage> faceShapes;

    vtkm::cont::ArrayHandle<vtkm::Id,OffsetsStorage> faceOffsets;
    vtkm::Id connectivitySize;
    timer.Reset();
    vtkm::cont::ConvertNumComponentsToOffsets(
          facePointCount, faceOffsets, connectivitySize);
    log.AddDictionaryEntry("seconds-face-point-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::Id,ConnectivityStorage> faceConnectivity;
    // Must pre allocate because worklet invocation will not have enough
    // information to.
    faceConnectivity.PrepareForOutput(connectivitySize, DeviceAdapter());

    vtkm::worklet::DispatcherMapField<BuildConnectivity, DeviceAdapter>
        buildConnectivityDispatcher(
          (BuildConnectivity(scatterCullInternalFaces)));

    timer.Reset();
    buildConnectivityDispatcher.Invoke(
          vtkm::cont::ArrayHandleIndex(totalNumFaces),
          inCellSet,
          originCells,
          originFaces,
          faceShapes,
          vtkm::cont::make_ArrayHandleGroupVecVariable(faceConnectivity,
                                                       faceOffsets));
    log.AddDictionaryEntry("seconds-build-connectivity",
                           timer.GetElapsedTime());

    outCellSet.Fill(inCellSet.GetNumberOfPoints(),
                    faceShapes,
                    facePointCount,
                    faceConnectivity,
                    faceOffsets);
  }

}; //struct ExternalFacesHashFightMorton


}} //namespace vtkm::worklet

#endif //vtk_m_worklet_ExternalFacesHashFightMorton_h
