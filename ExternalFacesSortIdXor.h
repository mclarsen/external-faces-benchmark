//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 Sandia Corporation.
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_worklet_ExternalFacesSortIdXor_h
#define vtk_m_worklet_ExternalFacesSortIdXor_h

#include <vtkm/CellShape.h>
#include <vtkm/Math.h>

#include <vtkm/exec/CellFace.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleIndex.h>
#include <vtkm/cont/ArrayHandlePermutation.h>
#include <vtkm/cont/ArrayHandleGroupVecVariable.h>
#include <vtkm/cont/CellSetExplicit.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/Field.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/worklet/DispatcherMapTopology.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>
#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/ScatterCounting.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/WorkletReduceByKey.h>

#include "YamlWriter.h"

namespace vtkm
{
namespace worklet
{

struct ExternalFacesSortIdXor
{
  // Given a cell shape, face index, and vec of cell connection ids,
  // returns an Id3 that is guaranteed to be unique and matching for
  // each face.
  template<typename CellShapeTag,
           typename CellNodeVecType>
  VTKM_EXEC
  static
  vtkm::Id3 CanonicalFaceId(CellShapeTag shape,
                            vtkm::IdComponent faceIndex,
                            const CellNodeVecType &cellNodeIds,
                            const vtkm::worklet::internal::WorkletBase *self)
  {
    vtkm::VecCConst<vtkm::IdComponent> localFaceIndices =
        vtkm::exec::CellFaceLocalIndices(faceIndex, shape, *self);

    VTKM_ASSERT(localFaceIndices.GetNumberOfComponents() >= 3);

    //Assign cell points/nodes to this face
    vtkm::Id faceP1 = cellNodeIds[localFaceIndices[0]];
    vtkm::Id faceP2 = cellNodeIds[localFaceIndices[1]];
    vtkm::Id faceP3 = cellNodeIds[localFaceIndices[2]];

    //Sort the first 3 face points/nodes in ascending order
    vtkm::Id sorted[3] = {faceP1, faceP2, faceP3};
    vtkm::Id temp;
    if (sorted[0] > sorted[2])
    {
      temp = sorted[0];
      sorted[0] = sorted[2];
      sorted[2] = temp;
    }
    if (sorted[0] > sorted[1])
    {
      temp = sorted[0];
      sorted[0] = sorted[1];
      sorted[1] = temp;
    }
    if (sorted[1] > sorted[2])
    {
      temp = sorted[1];
      sorted[1] = sorted[2];
      sorted[2] = temp;
    }

    // Check the rest of the points to see if they are in the lowest 3
    vtkm::IdComponent numPointsInFace =
        localFaceIndices.GetNumberOfComponents();
    for (vtkm::IdComponent pointIndex = 3;
         pointIndex < numPointsInFace;
         pointIndex++)
    {
      vtkm::Id nextPoint = cellNodeIds[localFaceIndices[pointIndex]];
      if (nextPoint < sorted[2])
      {
        if (nextPoint < sorted[1])
        {
          sorted[2] = sorted[1];
          if (nextPoint < sorted[0])
          {
            sorted[1] = sorted[0];
            sorted[0] = nextPoint;
          }
          else // nextPoint > P0, nextPoint < P1
          {
            sorted[1] = nextPoint;
          }
        }
        else // nextPoint > P1, nextPoint < P2
        {
          sorted[2] = nextPoint;
        }
      }
      else // nextPoint > P2
      {
        // Do nothing. nextPoint not in top 3.
      }
    }

    return vtkm::Id3(sorted[0], sorted[1], sorted[2]);
  }

  //Worklet that returns the number of faces for each cell/shape
  class NumFacesPerCell : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn inCellSet,
                                  FieldOut<> numFacesInCell);
    typedef _2 ExecutionSignature(CellShape);
    typedef _1 InputDomain;

    template<typename CellShapeTag>
    VTKM_EXEC
    vtkm::IdComponent operator()(CellShapeTag shape) const
    {
      return vtkm::exec::CellFaceNumberOfFaces(shape, *this);
    }
  };


  //Worklet that identifies a cell face by an XOR of all point ids. Not
  //necessarily completely unique.
  class FaceHash : public vtkm::worklet::WorkletMapPointToCell
  {
  public:
    typedef void ControlSignature(CellSetIn cellset,
                                  FieldOut<> faceHashes,
                                  FieldOut<> originCells,
                                  FieldOut<> originFaces);
    typedef void ExecutionSignature(_2,
                                    _3,
                                    _4,
                                    CellShape,
                                    FromIndices,
                                    InputIndex,
                                    VisitIndex);
    typedef _1 InputDomain;

    using ScatterType = vtkm::worklet::ScatterCounting;

    VTKM_CONT
    ScatterType GetScatter() const { return this->Scatter; }

    template<typename CountArrayType, typename Device>
    VTKM_CONT
    FaceHash(const CountArrayType &countArray, Device)
      : Scatter(countArray, Device())
    {
      VTKM_IS_ARRAY_HANDLE(CountArrayType);
    }

    VTKM_CONT
    FaceHash(const ScatterType &scatter)
      : Scatter(scatter)
    {  }

    template<typename CellShapeTag,
             typename CellNodeVecType>
    VTKM_EXEC
    void operator()(vtkm::UInt32 &faceHash,
                    vtkm::Id &cellIndex,
                    vtkm::IdComponent &faceIndex,
                    CellShapeTag shape,
                    const CellNodeVecType &cellNodeIds,
                    vtkm::Id inputIndex,
                    vtkm::IdComponent visitIndex) const
    {
      vtkm::VecCConst<vtkm::IdComponent> localFaceIndices =
          vtkm::exec::CellFaceLocalIndices(visitIndex, shape, *this);

      faceHash = 0;
      vtkm::IdComponent numPointsInFace =
          localFaceIndices.GetNumberOfComponents();
      for (vtkm::IdComponent pointIndex = 0;
           pointIndex < numPointsInFace;
           pointIndex++)
      {
        vtkm::IdComponent localId = localFaceIndices[pointIndex];
        // It is possible we are truncating the high order bits of the id.
        // Since the hash does not need to be perfect, we are going to live
        // with that.
        vtkm::UInt32 pointId = static_cast<vtkm::UInt32>(cellNodeIds[localId]);
        faceHash ^= pointId;
      }

      cellIndex = inputIndex;
      faceIndex = visitIndex;
    }

  private:
    ScatterType Scatter;
  };

  // Worklet that identifies the number of cells written out per face.
  // Because there can be collisions in the face ids, this instance might
  // represent multiple faces, which have to be checked. The resulting
  // number is the total number of external faces.
  class FaceCounts : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    typedef void ControlSignature(KeysIn keys,
                                  WholeCellSetIn<> inputCells,
                                  ValuesIn<> originCells,
                                  ValuesIn<> originFaces,
                                  ReducedValuesOut<> numOutputCells);
    typedef _5 ExecutionSignature(_2, _3, _4);
    using InputDomain = _1;

    template<typename CellSetType,
             typename OriginCellsType,
             typename OriginFacesType>
    VTKM_EXEC
    vtkm::IdComponent operator()(const CellSetType &cellSet,
                                 const OriginCellsType &originCells,
                                 const OriginFacesType &originFaces) const
    {
      vtkm::IdComponent numCellsOnHash = originCells.GetNumberOfComponents();
      VTKM_ASSERT(originFaces.GetNumberOfComponents() == numCellsOnHash);

      // Start by assuming all faces are unique, then remove one for each
      // face we find a duplicate for.
      vtkm::IdComponent numExternalFaces = numCellsOnHash;

      for (vtkm::IdComponent myIndex = 0;
           myIndex < numCellsOnHash-1; // Don't need to check last face
           myIndex++)
      {
        vtkm::Id3 myFace = CanonicalFaceId(
              cellSet.GetCellShape(originCells[myIndex]),
              originFaces[myIndex],
              cellSet.GetIndices(originCells[myIndex]),
              this);
        for (vtkm::IdComponent otherIndex = myIndex+1;
             otherIndex < numCellsOnHash;
             otherIndex++)
        {
          vtkm::Id3 otherFace = CanonicalFaceId(
                cellSet.GetCellShape(originCells[otherIndex]),
                originFaces[otherIndex],
                cellSet.GetIndices(originCells[otherIndex]),
                this);
          if (myFace == otherFace)
          {
            // Faces are the same. Must be internal. Remove 2, one for each
            // face.
            numExternalFaces -= 2;
            break;
          }
        }
      }

      return numExternalFaces;
    }
  };

  // Worklet that returns the number of points for each outputted face
  class NumPointsPerFace : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    typedef void ControlSignature(KeysIn keys,
                                  WholeCellSetIn<> inputCells,
                                  ValuesIn<> originCells,
                                  ValuesIn<> originFaces,
                                  ReducedValuesOut<> numPointsInFace);
    typedef _5 ExecutionSignature(_2, _3, _4, VisitIndex);
    using InputDomain = _1;

    using ScatterType = vtkm::worklet::ScatterCounting;

    VTKM_CONT
    ScatterType GetScatter() const { return this->Scatter; }

    template<typename CountArrayType, typename Device>
    VTKM_CONT
    NumPointsPerFace(const CountArrayType &countArray, Device)
      : Scatter(countArray, Device())
    {
      VTKM_IS_ARRAY_HANDLE(CountArrayType);
    }

    VTKM_CONT
    NumPointsPerFace(const ScatterType &scatter)
      : Scatter(scatter)
    {  }

    template<typename CellSetType,
             typename OriginCellsType,
             typename OriginFacesType>
    VTKM_EXEC
    vtkm::IdComponent operator()(const CellSetType &cellSet,
                                 const OriginCellsType &originCells,
                                 const OriginFacesType &originFaces,
                                 vtkm::IdComponent visitIndex) const
    {
      vtkm::IdComponent numCellsOnHash = originCells.GetNumberOfComponents();
      VTKM_ASSERT(originFaces.GetNumberOfComponents() == numCellsOnHash);

      // Find the visitIndex-th unique face.
      vtkm::IdComponent numFound = 0;
      vtkm::IdComponent myIndex = 0;
      while (true)
      {
        vtkm::Id3 myFace = CanonicalFaceId(
              cellSet.GetCellShape(originCells[myIndex]),
              originFaces[myIndex],
              cellSet.GetIndices(originCells[myIndex]),
              this);
        bool foundPair = false;
        for (vtkm::IdComponent otherIndex = myIndex+1;
             otherIndex < numCellsOnHash;
             otherIndex++)
        {
          vtkm::Id3 otherFace = CanonicalFaceId(
                cellSet.GetCellShape(originCells[otherIndex]),
                originFaces[otherIndex],
                cellSet.GetIndices(originCells[otherIndex]),
                this);
          if (myFace == otherFace)
          {
            // Faces are the same. Must be internal.
            foundPair = true;
            break;
          }
        }

        if (!foundPair)
        {
          if (numFound == visitIndex)
          {
            break;
          }
          else
          {
            numFound++;
          }
        }

        myIndex++;
      }

      return vtkm::exec::CellFaceNumberOfPoints(
            originFaces[myIndex],
            cellSet.GetCellShape(originCells[myIndex]),
            *this);
    }

  private:
    ScatterType Scatter;
  };

  // Worklet that returns the shape and connectivity for each external face
  class BuildConnectivity : public vtkm::worklet::WorkletReduceByKey
  {
  public:
    typedef void ControlSignature(KeysIn keys,
                                  WholeCellSetIn<> inputCells,
                                  ValuesIn<> originCells,
                                  ValuesIn<> originFaces,
                                  ReducedValuesOut<> shapesOut,
                                  ReducedValuesOut<> connectivityOut);
    typedef void ExecutionSignature(_2, _3, _4, VisitIndex, _5, _6);
    using InputDomain = _1;

    using ScatterType = vtkm::worklet::ScatterCounting;

    VTKM_CONT
    ScatterType GetScatter() const { return this->Scatter; }

    template<typename CountArrayType, typename Device>
    VTKM_CONT
    BuildConnectivity(const CountArrayType &countArray, Device)
      : Scatter(countArray, Device())
    {
      VTKM_IS_ARRAY_HANDLE(CountArrayType);
    }

    VTKM_CONT
    BuildConnectivity(const ScatterType &scatter)
      : Scatter(scatter)
    {  }

    template<typename CellSetType,
             typename OriginCellsType,
             typename OriginFacesType,
             typename ConnectivityType>
    VTKM_EXEC
    void operator()(const CellSetType &cellSet,
                    const OriginCellsType &originCells,
                    const OriginFacesType &originFaces,
                    vtkm::IdComponent visitIndex,
                    vtkm::UInt8 &shapeOut,
                    ConnectivityType &connectivityOut) const
    {
      vtkm::IdComponent numCellsOnHash = originCells.GetNumberOfComponents();
      VTKM_ASSERT(originFaces.GetNumberOfComponents() == numCellsOnHash);

      // Find the visitIndex-th unique face.
      vtkm::IdComponent numFound = 0;
      vtkm::IdComponent myIndex = 0;
      while (true)
      {
        vtkm::Id3 myFace = CanonicalFaceId(
              cellSet.GetCellShape(originCells[myIndex]),
              originFaces[myIndex],
              cellSet.GetIndices(originCells[myIndex]),
              this);
        bool foundPair = false;
        for (vtkm::IdComponent otherIndex = myIndex+1;
             otherIndex < numCellsOnHash;
             otherIndex++)
        {
          vtkm::Id3 otherFace = CanonicalFaceId(
                cellSet.GetCellShape(originCells[otherIndex]),
                originFaces[otherIndex],
                cellSet.GetIndices(originCells[otherIndex]),
                this);
          if (myFace == otherFace)
          {
            // Faces are the same. Must be internal.
            foundPair = true;
            break;
          }
        }

        if (!foundPair)
        {
          if (numFound == visitIndex)
          {
            break;
          }
          else
          {
            numFound++;
          }
        }

        myIndex++;
      }

      typename CellSetType::CellShapeTag shapeIn =
          cellSet.GetCellShape(originCells[myIndex]);
      shapeOut = vtkm::exec::CellFaceShape(originFaces[myIndex], shapeIn, *this);

      vtkm::VecCConst<vtkm::IdComponent> localFaceIndices =
          vtkm::exec::CellFaceLocalIndices(originFaces[myIndex], shapeIn, *this);
      vtkm::IdComponent numFacePoints =localFaceIndices.GetNumberOfComponents();
      VTKM_ASSERT(numFacePoints == connectivityOut.GetNumberOfComponents());

      typename CellSetType::IndicesType inCellIndices =
          cellSet.GetIndices(originCells[myIndex]);

      for (vtkm::IdComponent facePointIndex = 0;
           facePointIndex < numFacePoints;
           facePointIndex++)
      {
        connectivityOut[facePointIndex] =
            inCellIndices[localFaceIndices[facePointIndex]];
      }
    }

  private:
    ScatterType Scatter;
  };

public:

  ///////////////////////////////////////////////////
  /// \brief ExternalFacesSortIdXor: Extract Faces on outside of geometry
  template <typename InCellSetType,
            typename PointCoordsType,
            typename ShapeStorage,
            typename NumIndicesStorage,
            typename ConnectivityStorage,
            typename OffsetsStorage,
            typename DeviceAdapter>
  VTKM_CONT
  void Run(const InCellSetType &inCellSet,
           const PointCoordsType &vtkmNotUsed(pointCoords), // Remove if moved to VTK-m
           vtkm::cont::CellSetExplicit<
             ShapeStorage,
             NumIndicesStorage,
             ConnectivityStorage,
             OffsetsStorage> &outCellSet,
           YamlWriter &log,
           DeviceAdapter)
  {
    //Create a worklet to map the number of faces to each cell
    vtkm::cont::ArrayHandle<vtkm::IdComponent> facesPerCell;
    vtkm::worklet::DispatcherMapTopology<NumFacesPerCell,DeviceAdapter>
        numFacesDispatcher;

    vtkm::cont::Timer<DeviceAdapter> timer;
    numFacesDispatcher.Invoke(inCellSet, facesPerCell);
    log.AddDictionaryEntry("seconds-num-faces-per-cell",
                           timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCellToFace(facesPerCell, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-input-count", timer.GetElapsedTime());
    facesPerCell.ReleaseResources();

    if (scatterCellToFace.GetOutputRange(inCellSet.GetNumberOfCells()) == 0)
    {
      // Data has no faces. Output is empty.
      outCellSet.PrepareToAddCells(0, 0);
      outCellSet.CompleteAddingCells(inCellSet.GetNumberOfPoints());
      return;
    }

    vtkm::cont::ArrayHandle<vtkm::UInt32> faceHashes;
    vtkm::cont::ArrayHandle<vtkm::Id> originCells;
    vtkm::cont::ArrayHandle<vtkm::IdComponent> originFaces;
    vtkm::worklet::DispatcherMapTopology<FaceHash,DeviceAdapter>
        faceHashDispatcher((FaceHash(scatterCellToFace)));

    timer.Reset();
    faceHashDispatcher.Invoke(inCellSet, faceHashes, originCells, originFaces);
    log.AddDictionaryEntry("seconds-face-hash", timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::Keys<vtkm::UInt32> faceKeys(faceHashes,DeviceAdapter());
    log.AddDictionaryEntry("seconds-keys-build-arrays",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::IdComponent> faceOutputCount;
    vtkm::worklet::DispatcherReduceByKey<FaceCounts,DeviceAdapter>
        faceCountDispatcher;

    timer.Reset();
    faceCountDispatcher.Invoke(faceKeys,
                               inCellSet,
                               originCells,
                               originFaces,
                               faceOutputCount);
    log.AddDictionaryEntry("seconds-face-count", timer.GetElapsedTime());

    timer.Reset();
    vtkm::worklet::ScatterCounting
        scatterCullInternalFaces(faceOutputCount, DeviceAdapter());
    log.AddDictionaryEntry("seconds-face-output-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::IdComponent,NumIndicesStorage> facePointCount;
    vtkm::worklet::DispatcherReduceByKey<NumPointsPerFace,DeviceAdapter>
        pointsPerFaceDispatcher(scatterCullInternalFaces);

    timer.Reset();
    pointsPerFaceDispatcher.Invoke(faceKeys,
                                   inCellSet,
                                   originCells,
                                   originFaces,
                                   facePointCount);
    log.AddDictionaryEntry("seconds-points-per-face",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::UInt8,ShapeStorage> faceShapes;

    vtkm::cont::ArrayHandle<vtkm::Id,OffsetsStorage> faceOffsets;
    vtkm::Id connectivitySize;
    timer.Reset();
    vtkm::cont::ConvertNumComponentsToOffsets(
          facePointCount, faceOffsets, connectivitySize);
    log.AddDictionaryEntry("seconds-face-point-count",
                           timer.GetElapsedTime());

    vtkm::cont::ArrayHandle<vtkm::Id,ConnectivityStorage> faceConnectivity;
    // Must pre allocate because worklet invocation will not have enough
    // information to.
    faceConnectivity.Allocate(connectivitySize);

    vtkm::worklet::DispatcherReduceByKey<BuildConnectivity,DeviceAdapter>
        buildConnectivityDispatcher(scatterCullInternalFaces);

    timer.Reset();
    buildConnectivityDispatcher.Invoke(
          faceKeys,
          inCellSet,
          originCells,
          originFaces,
          faceShapes,
          vtkm::cont::make_ArrayHandleGroupVecVariable(faceConnectivity,
                                                       faceOffsets));
    log.AddDictionaryEntry("seconds-build-connectivity",
                           timer.GetElapsedTime());

    outCellSet.Fill(inCellSet.GetNumberOfPoints(),
                    faceShapes,
                    facePointCount,
                    faceConnectivity,
                    faceOffsets);
  }

}; //struct ExternalFacesSortIdXor


}} //namespace vtkm::worklet

#endif //vtk_m_worklet_ExternalFacesSortIdXor_h
